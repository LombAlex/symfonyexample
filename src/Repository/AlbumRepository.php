<?php

namespace App\Repository;

use App\Entity\Album;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AlbumRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Album::class);
    }

    /**
     * @return Album[]
     */
    public function findAllAlbum(): array
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT a
            FROM App\Entity\Album a'
        );

        // returns an array of Album objects
        return $query->getResult();
    }

    //There's already a find method inside ServiceEntityRepository
    // I made that as exemple   
    /**
     * @return Album
     */
    public function findAlbumById(int $id): Album
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT a
            FROM App\Entity\Album a
            WHERE a.id = :id'
        )->setParameter('id', $id);

        return $query->getResult()[0];
    }

    //Just a good exemple:
    // /**
    //  * @return Product[]
    //  */
    // public function findAllGreaterThanPrice(int $price): array
    // {
    //     $entityManager = $this->getEntityManager();

    //     $query = $entityManager->createQuery(
    //         'SELECT p
    //         FROM App\Entity\Product p
    //         WHERE p.price > :price
    //         ORDER BY p.price ASC'
    //     )->setParameter('price', $price);

    //     // returns an array of Product objects
    //     return $query->getResult();
    // }
}