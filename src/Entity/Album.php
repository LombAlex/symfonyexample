<?php

namespace App\Entity;

use App\Repository\AlbumRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=AlbumRepository::class)
 */
class Album
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Type("string")
     * @Assert\Type("string")
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "The title cannot be longer than {{ limit }} characters"
     * )
     */
    private $artist;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Type("string")
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "The artist cannot be longer than {{ limit }} characters"
     * )
     */
    private $title;

    public function getId(): ?int
    {
        return $this->id;
    }
    public function setId($pId)
    {
        $this->id = $pId;
    }

    public function getArtist(): ?string
    {
        return $this->artist;
    }
    public function setArtist($pArtist)
    {
        $this->artist = $pArtist;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }
    public function setTitle($pTitle)
    {
        $this->title = $pTitle;
    }
}