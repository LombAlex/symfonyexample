<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route; 
use Symfony\Component\HttpFoundation\Response; 
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class StudentController extends AbstractController { 
   //This is an annotation
   /** 
      * @Route("/student/home") 
   */ 
   public function homeAction() { 
      $name = 'Student details application'; 
      // return new Response( 
      //    '<html><body>Project: '.$name.'</body></html>' 
      // ); 
      return $this->render('student/home.html.twig'); 
   }

   /**      
      * @Route("/student/{page}", name = "student_about", requirements = {"page": "\d+"})
   */ 
   public function aboutAction($page = 1) { 
      return new Response($page); 
      //return $this->redirectToRoute('homepage');
   } 
}