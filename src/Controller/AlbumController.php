<?php

namespace App\Controller;

use App\Entity\Album;
use App\Form\AlbumForm;
use App\Form\DeleteForm;
use App\Repository\AlbumRepository;
use Doctrine\Persistence\ManagerRegistry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route; 
use Symfony\Component\HttpFoundation\Response; 
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AlbumController extends AbstractController
{
    /**
     * @Route("/album", name="index")
     */
    public function index(AlbumRepository $albumRepository): Response
    {
        $albums = $albumRepository->findAllAlbum();

        if (!$albums) {
            throw $this->createNotFoundException('No album found');
        }

        return $this->render('album/index.html.twig', ['albums' => $albums]);
    }

    /**
     * @Route("/album/add", name="add")
     */
    public function add(ManagerRegistry $doctrine, ValidatorInterface $validator, Request $request): Response
    {
        $album = new Album();
        //set a value by default
        //album->setTilte('Good title');

        $form = $this->createForm(AlbumForm::class, $album);
    
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //The isValid use the constraint of the class

            // $form->getData() holds the submitted values
            // but, the original `$album` variable has also been updated
            $album = $form->getData();

            $entityManager = $doctrine->getManager();
            $entityManager->persist($album);
            $entityManager->flush();

            return $this->redirectToRoute('index');
        }

        return $this->renderForm('album/add.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * @Route("/album/edit/{id}", name="edit")
     */
    public function edit(int $id, ManagerRegistry $doctrine, Request $request, AlbumRepository $albumRepository): Response
    {
        $album = $albumRepository->find($id);

        $form = $this->createForm(AlbumForm::class, $album);
    
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if($form->get('cancel')->isClicked()) {
                return $this->redirectToRoute('index');
            }

            $album = $form->getData();

            $entityManager = $doctrine->getManager();
            $entityManager->flush();

            return $this->redirectToRoute('index');
        }

        return $this->renderForm('album/add.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * @Route("/album/delete/{id}", name="delete")
     */
    public function delete(int $id, ManagerRegistry $doctrine, Request $request, AlbumRepository $albumRepository): Response
    {
        $album = $albumRepository->find($id);

        $form = $this->createForm(DeleteForm::class, $album);
    
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if($form->get('cancel')->isClicked()) {
                return $this->redirectToRoute('index');
            }
            $entityManager = $doctrine->getManager();
            $entityManager->remove($album);
            $entityManager->flush();

            return $this->redirectToRoute('index');
        }

        return $this->renderForm('album/delete.html.twig', [
            'form' => $form,
            'title' => $album->getTitle()
        ]);
    }

    /**
     * @Route("/album/{id}", name="album_show")
     */
    public function show(int $id, AlbumRepository $albumRepository): Response
    {
        //$album = $albumRepository->find($id);
        $album = $albumRepository->findAlbumById($id);

        if (!$album) {
            throw $this->createNotFoundException(
                'No album found for id '.$id
            );
        }

        return new Response('Check out this great album: '.$album->getArtist());

        // or render a template
        // in the template, print things with {{ album.artist }}
        // return $this->render('album/show.html.twig', ['album' => $album]);
    }
}