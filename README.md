# Symfony Framework PHP example 2022

## About

I have created this project to learn how Symfony work.

## What I've done

- Connect the project to a database and display the content
- Create a form, then add/edit/delete an element in the database

## Tutos followed

https://www.tutorialspoint.com/symfony/symfony_creating_simple_web_application.htm
and 
https://symfony.com/doc/5.4/setup.html

## Installation on linux (based on Arch)

Install with pacman php + symfony-cli

https://www.apachefriends.org/download.html download run file here

```
Php:
7.4.28

Xamp:
port 80
folder /opt/lampp/htdocs/

Mysql port 3306
http://localhost/phpmyadmin/

Symfony 5.4.8
```

Launch appache server (and Mysql):
```
sudo /opt/lampp/manager-linux-x64.run
```

Add Composer to Xampp (Composer is like Node in js)
```
curl -s https://getcomposer.org/installer | /opt/lampp/bin/php
```
(I've installed it inside /opt/lampp/htdocs/Laminas)

Call the php and composer from Xampp:
```
/opt/lampp/bin/php /opt/lampp/htdocs/Laminas/composer.phar
```

## Create Project

Create the project
```
/opt/lampp/bin/php /opt/lampp/htdocs/Laminas/composer.phar create-project symfony/skeleton ./symfonyexample/
```
Add few package for a web application
```
/opt/lampp/bin/php /opt/lampp/htdocs/Laminas/composer.phar require symfony/webapp-pack

/opt/lampp/bin/php /opt/lampp/htdocs/Laminas/composer.phar require symfony/webapp-meta
```
Then enter in your directory then
```
/opt/lampp/bin/php /opt/lampp/htdocs/Laminas/composer.phar install
```

Create a file .php-version with the php version that you want to execute. You can check your different php versions.
```
symfony local:php:list
```

And finally, to run the server
```
symfony server:start --port=8080
```

http://localhost:8080/

## Others Commands

Check commands like 
```
php bin/console make:controller ProductController
```

## Shortcuts

```
symfony server:start --port=8080
```

http://localhost:8080/

```
sudo /opt/lampp/manager-linux-x64.run
```

/opt/lampp/bin/php /opt/lampp/htdocs/Laminas/composer.phar

Doc 

https://symfony.com/doc/5.4/index.html
https://github.com/symfony/symfony

## Tips

Use the "annotations" module (composer) to define route inside the controller in PHP < 8. 
The module "twig" to return HTML.
To connect to a database you need the module doctrine.

<br>

Use to debug
```
php bin/console
or
/opt/lampp/bin/php bin/console
```

For example: ```/opt/lampp/bin/php bin/console debug:router``` show every route of the project

<br>

config/
    To configure routes, services and packages. 
src/
    All your PHP code lives here. 
public/
    Publicly accessible files here.
templates/
    All your Twig templates live here. 
var/
    This is where automatically-created files are stored.

<br>

Twig contains three types of special syntax:

- {{ ... }} − Prints a variable or the result of an expression to the template.

- {% ... %} − A tag that controls the logic of the template. It is mainly used to execute a function.

- {# ... #} − Comment syntax. It is used to add a single or multi-line comments.

<br>

To connect to a database: .env file.

<br>

